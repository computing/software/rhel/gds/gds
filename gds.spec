%define name 	gds
%define version 2.19.2
%define release 1
%define daswg   /usr
%define prefix  %{daswg}
%define dmtrun 1
%if %{dmtrun}
%define _sysconfdir /etc
%define config_runflag --enable-dmt-runtime --sysconfdir=%{_sysconfdir}
%else
%define config_runflag --disable-dmt-runtime
%endif
%define withpygds 1
%if %{withpygds}
%define config_pygds --enable-python
%else
%define config_pygds --disable-python
%endif
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	GDS 2.19.2
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LIGO Global Diagnotic Systems
Source: 	https://software.igwn.org/lscsoft/source/%{name}-%{version}.tar.gz
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gzip zlib bzip2 expat-devel ldas-tools-framecpp
BuildRequires:  ldas-tools-framecpp-devel ldas-tools-al-devel libmetaio-devel
BuildRequires:  libcurl-devel zlib-devel krb5-devel
BuildRequires:  readline-devel fftw-devel cyrus-sasl-devel
BuildRequires:  jsoncpp-devel
BuildRequires:  boost-devel
AutoReqProv: 	no
Provides: 	%name
Obsoletes:	%name < %version
Prefix:		%prefix

%description
Global diagnostics software

%package base
Summary: 	GDS package Core libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       expat fftw cyrus-sasl
Obsoletes:      %{name}-core <= 2.18.19
Obsoletes:      %{name}-all <= 2.18.19

%description base
Core libraries required by the rest of the GDS packages

%package framexmit
Summary: 	GDS package low-latency libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-base = %{version}-%{release}
Obsoletes:      %{name}-core < 2.18
Obsoletes:      %{name}-services < 2.18

%description framexmit
Lowlatency libraries implement the gds shared memory and interconnects.

%package framexmit-headers
Summary: 	GDS header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-base-headers = %{version}-%{release}

%description framexmit-headers
 GDS software header files.

%package framexmit-devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-framexmit = %{version}-%{release}
Requires:       %{name}-framexmit-headers = %{version}-%{release}
Requires:       %{name}-base-devel = %{version}-%{release}

%description framexmit-devel
 GDS software development files.

%package base-frameio
Summary: 	GDS package Frame I/O libraries based on
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-base = %{version}-%{release}
Requires:       %{name}-framexmit = %{version}-%{release}
Requires:       ldas-tools-framecpp
Obsoletes:      %{name}-core < 2.18

%description base-frameio
Frame I/O files libraries

%package base-crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-base = %{version}-%{release}
Requires:       %{name}-base-headers = %{version}-%{release}
Requires:       libcurl

%description base-crtools
GDS control room tools

%package base-devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-framexmit-devel = %{version}-%{release}
Requires:       %{name}-base-frameio = %{version}-%{release}
Requires:       %{name}-services = %{version}-%{release}
Requires:       %{name}-base-headers = %{version}-%{release}
Requires:       expat-devel, cyrus-sasl-devel, ldas-tools-framecpp-devel

%description base-devel
GDS software development files.

%package base-headers
Summary: 	GDS header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-framexmit-headers = %{version}-%{release}
Requires:       boost-devel
Obsoletes:      %{name}-devel < 2.18

%description base-headers
GDS software header files.

%package base-root
Summary: 	 Root wrappers for gds libraries
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
Requires:  %{name}-base = %{version}-%{release}
Obsoletes: %{name}-core-root < 2.18

%description base-root
Root wrappers for gds class libraries and macros/classes for inteactive use
of gds libraries via root.

%package  base-monitors
Summary: 	Libraries used by DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name}-base = %{version}-%{release}

%description base-monitors
Libraries used by DMT Monitor programs

%package   services
Summary: 	 GDS services
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
Requires:  %{name}-base = %{version}-%{release}
Requires:  %{name}-base-monitors = %{version}-%{release}
Obsoletes: %{name}-core < 2.18

%description services
GDS runtime services

%if %dmtrun
%package base-runtime
Summary: 	DMT run-time software
Version: 	%{version}
Group: 		LSC Software/Data Analysis

%description base-runtime
DMT run-time supervisor and services
%endif

%package base-web
Summary: 	DMT web services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-base = %{version}-%{release}, libcurl

%description base-web
DMT web services

%prep
%setup -q

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

export PKG_CONFIG_PATH ROOTSYS
./configure  --prefix=%prefix --libdir=%{_libdir} \
	     --includedir=%{prefix}/include/%{name} \
	     --enable-online --enable-dtt %{config_pygds} %{config_runflag}
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files base
%defattr(-,root,root)
%{_libdir}/libdaqs.so*
%{_libdir}/libdmtsigp.so*
%{_libdir}/libframefast.so*
%{_libdir}/libframeutil.so*
%{_libdir}/libgdsbase.so*
%{_libdir}/libgdscntr.so*
%{_libdir}/libgdsmath.so*
%{_libdir}/libhtml.so*
%{_libdir}/libjsstack.so*
%{_libdir}/liblmsg.so*
%{_libdir}/liblxr.so*
%{_libdir}/libparsl.so*
%{_libdir}/libsockutil.so*
%{_libdir}/libweb.so*
%{_libdir}/libxsil.so*
%{_datadir}/gds/html-styles

%files framexmit
%{_libdir}/libframexmit.so*

%files base-frameio
%{_bindir}/cache_util
%{_bindir}/fdir
%{_bindir}/fextract
%{_bindir}/finfo
%{_bindir}/framedump
%{_bindir}/fsettime
#%{_libdir}/libframeio.so*

%files base-crtools
%defattr(-,root,root)
%{_libdir}/libclient.so*
%{_libdir}/libgdsalgo.so*
%{_libdir}/libdmtaccess.so*
%{_libdir}/libmondmtsrvr.so*
%{_libdir}/libmonlmsg.so*

%files framexmit-headers
%defattr(-,root,root)
%{_includedir}/%{name}/framexmit

%files framexmit-devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/gds-framexmit.pc
%{_libdir}/libframexmit.a

%files base-devel
%defattr(-,root,root)
%exclude %{_libdir}/pkgconfig/gds-framexmit.pc
%exclude %{_libdir}/libframexmit.a
%{_libdir}/pkgconfig/*
%{_libdir}/*.a

%files base-headers
%defattr(-,root,root)
%exclude %{_includedir}/%{name}/framexmit
%{_includedir}/%{name}/*

%files services
%defattr(-,root,root)
%{_bindir}/Alarm
%{_bindir}/AlarmCtrl
%{_bindir}/AlarmMgr
%{_bindir}/NameCtrl
%{_bindir}/NameServer
%{_bindir}/TrigMgr
%{_bindir}/trigRmNode
%{_bindir}/TrigRndm
%{_libdir}/libgdstrig.so*
%{_libdir}/libserver.so*

%files base-monitors
%defattr(-,root,root)
%{_libdir}/libtclient.so*
%{_libdir}/libmonitor.so*

%if %dmtrun
%defattr(-,root,root)
%files base-runtime
%defattr(-,root,root)
%{_bindir}/no_insert
%{_datadir}/%name/procmgt
%endif

%files base-web
%defattr(-,root,root)
%{_bindir}/webview
%{_bindir}/webxmledit

%files base-root
%defattr(-,root,root)
%{_libdir}/libgdsevent.so*

%changelog
* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
